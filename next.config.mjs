/** @type {import('next').NextConfig} */
const nextConfig = {

    images: {
        // loader: "imgix",
        // loaderFile: "./src/loader.js",
        remotePatterns: [
          {
            hostname: "assets.cogeter.com",
          },
          {
            hostname: "https://192.168.18.252",
          },
          {
            hostname: "medyoldapi.aiksol.com",
          },
          {
            hostname: "localhost",
          },
        ],
      },
    
};

export default nextConfig;
