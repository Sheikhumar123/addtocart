"use client";

import ProductDetail from "@/components/ProductDetail/ProductDetail";
import { Box, CircularProgress } from "@mui/material";
import axios from "axios";
import { useSearchParams } from "next/navigation";
import { useEffect, useState } from "react";

export default function Product() {
  const searchParams = useSearchParams();
  const slug = searchParams.get("slug");

  const [productDetail, setproductDetail] = useState(null);

  useEffect(() => {
    if (slug) {
      handelProduct();
    }
  }, [slug]);
  const handelProduct = async () => {
    try {
      let response = await axios.get(
        `https://store-api.cogeter.com/api/products/${slug}`
      );

      setproductDetail(response.data.data);
    } catch (error) {
      return [];
    }
  };

  return (
    <main>
      {productDetail ? (
        <ProductDetail productDetail={productDetail} />
      ) : (
        <Box
          sx={{
            width: "100%",
            height: "90vh",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <CircularProgress />
        </Box>
      )}
    </main>
  );
}
