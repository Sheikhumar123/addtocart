
import axios from "axios";
import Products from "@/components/Products/Products";

export default async function Home() {
  const AllProducts = await getProducts();

  return (
    <main>
      <Products AllProducts={AllProducts} />
    </main>
  );
}

const getProducts = async () => {
  try {
    let response = await axios.get(
      "https://store-api.cogeter.com/api/products?limit=30"
    );

    return response.data.data;
  } catch (error) {
    return [];
  }
};
