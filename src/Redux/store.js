"use client";
import { configureStore } from "@reduxjs/toolkit";
import cartSlice from "./slices/cartSlice";
import productSlice from "./slices/productSlice";

const ReduxStore = configureStore({
  reducer: {
    cart: cartSlice,
    product: productSlice,
  },
});

export default ReduxStore;
