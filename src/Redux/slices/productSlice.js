"use client";
import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  message: "Test",
  AllProducts: [],
  error: null,
  status: "idle", //pending,success,failed
};

const ProductsSlice = createSlice({
  name: "ProductsSlice",
  initialState,
  reducers: {
    setAllProducts: (state, action) => {
      state.AllProducts = action.payload;
    },
  },
  extraReducers: () => {},
});
export const { setAllProducts } = ProductsSlice.actions;

export default ProductsSlice.reducer;
