"use client";
import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  message: "Test",
  CartData: [],
  error: null,
  status: "idle", //pending,success,failed
};

const cartSlice = createSlice({
  name: "cartSlice",
  initialState,
  reducers: {
    addToCart: (state, action) => {
      let alreadyExist = state.CartData.find((product) => {
        return product?.identifier == action.payload?.identifier;
      });
      if (alreadyExist) {
        alreadyExist.quantity += 1;
      } else {
        state.CartData.push(action.payload);
      }
    },
    removeFromCart: (state, action) => {
      state.CartData = state.CartData.filter(
        (product) => product?.cart_id !== action?.payload
      );
    },
    changeQty: (state, action) => {
      let alreadyExist = state.CartData.find((product) => {
        return product?.cart_id == action.payload?.cart_id;
      });

      alreadyExist.quantity += action.payload?.quantity;
    },
  },
  extraReducers: () => {},
});
export const { addToCart, removeFromCart, changeQty } = cartSlice.actions;
export default cartSlice.reducer;
