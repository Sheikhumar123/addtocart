"use client"
import { Grid } from '@mui/material';
import React, { useEffect } from 'react'
import ProductCard from '../ProductCard/ProductCard';
import { useDispatch } from 'react-redux';
import { setAllProducts } from '@/Redux/slices/productSlice';
import Link from 'next/link';

const Products = ({AllProducts}) => {
    const dispatch  = useDispatch()
    useEffect(() => {
      dispatch(setAllProducts(AllProducts))
    }, [AllProducts])
    
  return (
    <Grid container spacing={4} sx={{
        padding:{
          xs:"10px" , sm:"15px" , md:"20px" , lg:"25px"
        }
      }}>
        {AllProducts?.map((product) => {
          return (
            <Grid item xs={6} sm={4} lg={3}>
                <Link href={`product?slug=${product?.identifier}`}>
              <ProductCard product={product} />
                </Link>
            </Grid>
          );
        })}
      </Grid>
  )
}

export default Products