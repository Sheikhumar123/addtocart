import { Box, Typography } from "@mui/material";
import Image from "next/image";
import React from "react";
import FavoriteBorderIcon from "@mui/icons-material/FavoriteBorder";
import FavoriteIcon from "@mui/icons-material/Favorite";
import "react-inner-image-zoom/lib/InnerImageZoom/styles.css";
import ReactImageMagnify from "react-image-magnify";

const ProductCard = ({ product }) => {
  return (
    <Box>
      <Box
        sx={{
          position: "relative",
        }}
      >
        <Image
          src={product?.gallery[0]?.url}
          height="500"
          width="100"
          style={{
            width: "100%",
            height: "auto",
          }}
        />
        {/* <InnerImageZoom
          src={product?.gallery[0]?.url}
          zoomType="hover"
          zoomPreload
          alt="Zoomed Image"
          zoomSrc={product?.gallery[0]?.url}
        /> */}
        {/* <ReactImageMagnify
          {...{
            smallImage: {
              alt: "Wristwatch by Ted Baker London",
              isFluidWidth: true,
              src: product?.gallery[0]?.url,
            },
            largeImage: {
              src: product?.gallery[0]?.url,
              width: 1200,
              height: 1800,
            },
          }}
        /> */}
        

        <Box
          sx={{
            backgroundColor: product?.on_sale ? "white" : "black",
            color: product?.on_sale ? "black" : "white",
            position: "absolute",
            top: 10,
            left: 10,
            padding: "5px",
            fontSize: "14px",
          }}
        >
          {product?.on_sale ? "ON SALE" : "SOLD OUT"}
        </Box>
        <Box
          sx={{
            backgroundColor: "lightgrey",
            color: "white",
            position: "absolute",
            top: 10,
            right: 10,
            padding: "5px 5px 0px 5px",
            fontSize: "14px",
          }}
        >
          {product?.is_wishlist ? <FavoriteIcon /> : <FavoriteBorderIcon />}
        </Box>
      </Box>
      <Box>
        <Typography
          sx={{
            fontSize: { xs: "12px", sm: "12px", md: "14px", lg: "16px" },
          }}
        >
          {product?.title}
        </Typography>
        <Box
          sx={{
            display: "flex",
            alignItem: "center",
            gap: "20px",
          }}
        >
          <Typography
            sx={{
              fontSize: { xs: "12px", sm: "14px", md: "16px", lg: "18px" },

              fontWeight: "600",
            }}
          >
            {product?.on_sale ? product?.sale_price : product?.regular_price}{" "}
            AED
          </Typography>
          {product?.on_sale ? (
            <Typography
              sx={{
                textDecoration: "line-through",
                color: "grey",
                fontSize: { xs: "12px", sm: "14px", md: "16px", lg: "18px" },
              }}
            >
              { product?.regular_price}{" "}
              AED
            </Typography>
          ) : null}
        </Box>
      </Box>
    </Box>
  );
};

export default ProductCard;
