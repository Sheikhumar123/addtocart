import {
  Box,
  Button,
  CardMedia,
  Divider,
  Grid,
  Typography,
} from "@mui/material";
import Image from "next/image";
import React, { useEffect, useState } from "react";
import FavoriteBorderIcon from "@mui/icons-material/FavoriteBorder";
import ShareIcon from "@mui/icons-material/Share";
import { useDispatch } from "react-redux";
import { addToCart } from "@/Redux/slices/cartSlice";
const ProductDetail = ({ productDetail }) => {
  const [selecetdSize, setselecetdSize] = useState("");
  const dispatch = useDispatch();
  useEffect(() => {
    let available = productDetail?.variants?.find((size) => {
      return size?.quantity > 0;
    });
    setselecetdSize(available?.size);
  }, [productDetail]);

  const handelAddToCart = () => {
    console.log(new Date().getTime().toString());
    dispatch(
      addToCart({
        cart_id: new Date().getTime().toString(),
        identifier:productDetail?.identifier,
        title: productDetail?.title,
        coverImage: productDetail?.cover_image,
        price: productDetail?.on_sale
          ? productDetail?.sale_price
          : productDetail?.regular_price,
        size: selecetdSize,
        quantity:1,
        regular_price: productDetail?.regular_price,
        sale_price: productDetail?.sale_price,
      })
    );
  };

  console.log(productDetail);
  return (
    <Box
      sx={{
        padding: "30px",
      }}
    >
      <Grid container spacing={2}>
        <Grid xs={12} sm={7} md={7} lg={7} spacing={2} sx={{}}>
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              gap: "20px",
            }}
          >
            <Box
              sx={{
                width: "100%",
                display: "flex",
                justifyContent: "center",
              }}
            >
              <Image
                src={productDetail?.cover_image}
                height="500"
                width="400"
                style={{
                  width: "80%",
                  maxWidth: "500px",
                  height: "auto",
                }}
              />
            </Box>
            <Box
              sx={{
                width: "100%",
                display: "flex",
                justifyContent: "center",
              }}
            >
              <CardMedia
                src={productDetail?.video}
                autoPlay={true}
                component={"video"}
                loop
                sx={{
                  width: "80%",
                  maxWidth: "500px",

                  height: "auto",
                }}
                controls
              />
            </Box>

            {productDetail?.gallery?.map((image) => {
              return (
                <Box
                  key={image?.url}
                  sx={{
                    width: "100%",
                    display: "flex",
                    justifyContent: "center",
                  }}
                >
                  <Image
                    src={image?.url}
                    height="500"
                    width="400"
                    style={{
                      width: "80%",
                      maxWidth: "500px",

                      height: "auto",
                    }}
                  />
                </Box>
              );
            })}
          </Box>
        </Grid>
        <Grid xs={12} sm={5} md={5} lg={5}>
          <Box
            sx={{
              display: "flex",
              justifyContent: "start",
            }}
          >
            <Box
              sx={{
                width: "100%",
                maxWidth: "450px",
                display: "flex",
                flexDirection: "column",
                gap: "15px",
              }}
            >
              <Typography
                sx={{
                  color: "rgb(112, 137, 251)",
                  cursor: "pointer",
                }}
              >
                {productDetail?.category?.title}
              </Typography>
              <Box
                sx={{
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "center",
                  gap: "10px",
                  flexWrap: "wrap",
                }}
              >
                <Typography
                  sx={{
                    fontSize: "20px",
                  }}
                >
                  {productDetail?.title}
                </Typography>
                <Box
                  sx={{
                    display: "flex",
                    gap: "10px",
                    alignItems: "center",
                  }}
                >
                  <FavoriteBorderIcon />
                  <ShareIcon />
                </Box>
              </Box>
              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  gap: "10px",
                  justifyContent: "space-between",
                }}
              >
                <Box
                  sx={{
                    display: "flex",
                    alignItems: "center",
                    gap: "20px",
                  }}
                >
                  {productDetail?.on_sale ? (
                    <Typography
                      sx={{
                        textDecoration: "line-through",
                        color: "grey",
                        fontSize: {
                          xs: "12px",
                          sm: "14px",
                          md: "16px",
                          lg: "18px",
                        },
                        color: "#888",
                      }}
                    >
                      {productDetail?.regular_price} AED
                    </Typography>
                  ) : null}
                  <Typography
                    sx={{
                      fontSize: {
                        xs: "12px",
                        sm: "14px",
                        md: "16px",
                        lg: "18px",
                      },

                      color: "#95a8fc",
                      fontWeight: "700",
                      fontSize: "20px",
                    }}
                  >
                    {productDetail?.on_sale
                      ? productDetail?.sale_price
                      : productDetail?.regular_price}{" "}
                    AED
                  </Typography>
                </Box>
                {productDetail?.on_sale ? (
                  <Typography
                    sx={{
                      fontSize: "14px",
                    }}
                  >
                    {(
                      ((productDetail?.regular_price -
                        productDetail?.sale_price) /
                        productDetail?.regular_price) *
                      100
                    ).toFixed(2)}
                    % OFF
                  </Typography>
                ) : null}
              </Box>
              <Divider />
              <Box>
                <Typography
                  sx={{
                    fontSize: "14px",
                    fontWeight: "500",
                  }}
                >
                  Available Colors
                </Typography>
                <Box
                  sx={{
                    display: "flex",
                    width: "100%",
                    gap: "15px",
                    flexWrap: "wrap",
                  }}
                >
                  {productDetail?.similar_products?.map((product) => {
                    return (
                      <Box
                        sx={{
                          border: "1px solid black",
                          padding: "5px",
                        }}
                      >
                        <Image src={product?.url} height="65" width="65" />
                      </Box>
                    );
                  })}
                </Box>
              </Box>
              <Divider />
              <Box
                sx={{
                  paddingTop: "10px",
                }}
              >
                <Box
                  sx={{
                    display: "flex",
                    justifyContent: "space-between",
                    alignItems: "center",
                  }}
                >
                  <Typography>Size</Typography>
                  <Typography>Size Chart</Typography>
                </Box>

                <Box
                  sx={{
                    display: "flex",
                    alignItems: "center",
                    gap: "10px",
                  }}
                >
                  {productDetail?.variants?.map((size) => {
                    return (
                      <Button
                        sx={{
                          border: "1px solid black",
                          padding: "5px",
                          width: "50px",
                          height: "40px",
                          borderRadius: "5px",
                          display: "flex",
                          alignItems: "center",
                          justifyContent: "center",
                          position: "relative",
                          backgroundColor: "ransparent",
                          border:
                            selecetdSize == size?.size
                              ? "2px solid #95a8fc"
                              : "2px solid black",
                        }}
                        onClick={() => {
                          if (size?.quantity > 0) {
                            setselecetdSize(size?.size);
                          } else {
                            console.log("size not available");
                          }
                        }}
                      >
                        <Typography>{size?.size}</Typography>
                        {size?.quantity < 1 ? (
                          <Box
                            sx={{
                              "::before": {
                                content: '""',
                                display: "block",
                                position: "absolute",
                                top: 0,
                                right: 0,
                                bottom: 0,
                                left: 0,
                                zIndex: -1,
                                background:
                                  "linear-gradient(to bottom left, black 49.5%, red 49.5%, transparent 50.5%, black 50.5%)",
                              },
                            }}
                          ></Box>
                        ) : null}
                      </Button>
                    );
                  })}
                </Box>
              </Box>
              <Box
                sx={{
                  width: "100%",
                  justifyContent: "center",
                }}
              >
                <Button
                  sx={{
                    width: "100%",
                    color: "white",
                    textTransform: "capitalize",
                    background:
                      "#0000 linear-gradient(270deg,#7089fb,#aa41fa) 0 0 no-repeat padding-box",
                  }}
                  onClick={handelAddToCart}
                >
                  Add To cart
                </Button>
              </Box>
            </Box>
          </Box>
        </Grid>
      </Grid>
    </Box>
  );
};

export default ProductDetail;
