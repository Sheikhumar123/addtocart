import {
  Box,
  Button,
  Divider,
  IconButton,
  Modal,
  Typography,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import CancelOutlinedIcon from "@mui/icons-material/CancelOutlined";
import Image from "next/image";
import { changeQty, removeFromCart } from "@/Redux/slices/cartSlice";
import DeleteForeverOutlinedIcon from "@mui/icons-material/DeleteForeverOutlined";
const CartModal = ({ open, handleClose }) => {
  const { CartData } = useSelector((state) => state?.cart);
  const [SubTotal, setSubTotal] = useState(0);
  const dispatch = useDispatch();
  console.log(CartData);

  const handelQuantity = (cart_id, quantity) => {
    dispatch(
      changeQty({
        quantity,
        cart_id,
      })
    );
  };
  const handelDelete = (cart_id) => {
    dispatch(removeFromCart(cart_id));
  };

  useEffect(() => {
    let TotalPrice = 0;
    CartData?.map((product) => {
      let price = product?.sale_price
        ? product?.sale_price
        : product?.regular_price;
      TotalPrice += price * product?.quantity;
    });
    setSubTotal(TotalPrice);
  }, [CartData]);

  return (
    <Modal
      open={open}
      onClose={handleClose}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <Box
        sx={{
          display: "flex",
          width: "100vw",
          height: "100vh",
          justifyContent: "flex-end",
          alignItems: "center",
        }}
      >
        <Box
          sx={{
            width: "100%",
            maxWidth: "475px",
            height: "100%",
            overflow: "scroll",
            backgroundColor: "whitesmoke",
            padding: "10px 15px",
          }}
        >
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
            }}
          >
            <Box
              sx={{
                display: "flex",
                alignItems: "center",
                gap: "5px",
              }}
            >
              <Typography
                sx={{
                  fontWeight: "600",
                }}
              >
                {CartData.length}
              </Typography>{" "}
              <Typography>Product added to your basket</Typography>{" "}
            </Box>
            <IconButton
              size="large"
              aria-label="account of current user"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              color="inherit"
              onClick={() => {
                handleClose(false);
              }}
            >
              <CancelOutlinedIcon />
            </IconButton>
          </Box>
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              gap: "10px",
            }}
          >
            {CartData?.map((product) => {
              return (
                <Box
                  sx={{
                    display: "flex",
                    justifyContent: "start",
                    gap: "10px",
                    alignItems: "start",
                  }}
                >
                  <Box
                    sx={{
                      padding: "5px",
                    }}
                  >
                    <Image
                      src={product?.coverImage}
                      height="85"
                      width="70"
                      style={{
                        width: "85px",
                        height: "120px",
                      }}
                    />
                  </Box>
                  <Box>
                    <Typography>{product?.title}</Typography>
                    <Box
                      sx={{
                        display: "flex",
                        justifyContent: "flex-start",
                        gap: "10px",
                        alignItems: "center",
                      }}
                    >
                      <Typography
                        sx={{
                          fontSize: {
                            xs: "12px",
                            sm: "14px",
                            md: "16px",
                          },

                          color: "#95a8fc",
                          fontWeight: "700",
                        }}
                      >
                        {product?.sale_price
                          ? product?.sale_price
                          : product?.regular_price}{" "}
                        AED
                      </Typography>
                      {product?.sale_price ? (
                        <Typography
                          sx={{
                            textDecoration: "line-through",
                            color: "grey",
                            fontSize: {
                              xs: "12px",
                              sm: "14px",
                              md: "16px",
                            },
                            color: "#888",
                          }}
                        >
                          {product?.regular_price} AED
                        </Typography>
                      ) : null}
                    </Box>
                    <Box
                      sx={{
                        border: "1px solid black",
                        padding: "5px",
                        width: "30px",
                        height: "30px",
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "center",
                        position: "relative",
                        backgroundColor: "ransparent",
                        border: "0.5px solid black",
                      }}
                    >
                      <Typography
                        sx={{
                          fontSize: "12px",
                        }}
                      >
                        {product?.size}
                      </Typography>
                    </Box>
                    <Box
                      sx={{
                        display: "flex",
                        width: "100%",
                        justifyContent: "space-between",
                        alignItems: "center",
                      }}
                    >
                      <Box
                        sx={{
                          border: "1px solid black",
                          padding: "5px",
                          width: "100px",
                          height: "30px",
                          display: "flex",
                          alignItems: "center",
                          justifyContent: "space-between",
                          position: "relative",
                          backgroundColor: "ransparent",
                          border: "0.5px solid black",
                        }}
                      >
                        <IconButton
                          size="large"
                          aria-label="account of current user"
                          aria-controls="menu-appbar"
                          aria-haspopup="true"
                          color="inherit"
                          disabled={product?.quantity < 2}
                          onClick={() => {
                            handelQuantity(product?.cart_id, -1);
                          }}
                        >
                          <Typography
                            sx={{
                              fontSize: "14px",
                            }}
                          >
                            -
                          </Typography>
                        </IconButton>
                        <Typography
                          sx={{
                            fontSize: "14px",
                            fontWeight: "600",
                          }}
                        >
                          {product?.quantity}
                        </Typography>
                        <IconButton
                          size="large"
                          aria-label="account of current user"
                          aria-controls="menu-appbar"
                          aria-haspopup="true"
                          color="inherit"
                          onClick={() => {
                            handelQuantity(product?.cart_id, 1);
                          }}
                        >
                          <Typography
                            sx={{
                              fontSize: "14px",
                            }}
                          >
                            +
                          </Typography>
                        </IconButton>
                      </Box>
                      <IconButton
                        size="large"
                        aria-label="account of current user"
                        aria-controls="menu-appbar"
                        aria-haspopup="true"
                        color="inherit"
                        onClick={() => {
                          handelDelete(product?.cart_id);
                        }}
                      >
                        <DeleteForeverOutlinedIcon />
                      </IconButton>
                    </Box>
                  </Box>
                </Box>
              );
            })}
          </Box>
          <Box
            sx={{
              width: "100%",
              display:"flex",
              flexDirection:"column",
              justifyContent: "center",
              paddingTop: "25px",
              gap:"20px"
            }}
          >
            <Divider />
            <Box
              sx={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
              }}
            >
              <Typography sx={{
                fontWeight:"600"
              }}>Sub Total</Typography>
              <Typography>{SubTotal} AED</Typography>
            </Box>
            <Divider />
            <Button
              sx={{
                width: "100%",
                color: "white",
                fontWeight: "600",
                textTransform: "capitalize",
                background:
                  "#0000 linear-gradient(270deg,#7089fb,#aa41fa) 0 0 no-repeat padding-box",
              }}
            >
              CheckOut
            </Button>
          </Box>
        </Box>
      </Box>
    </Modal>
  );
};

export default CartModal;
